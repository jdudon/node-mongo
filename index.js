const express = require('express');
const bodyParser = require('body-parser');
const firstRouter = require('./first-router');



const app = express();

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended:true}));

const router = express.Router();
app.use('/api', router);
app.use('/dog', firstRouter);

router.get('/', function (request, response) { response.json({test:"pouit"});
})

app.listen(3000);