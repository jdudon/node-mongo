const router = require('express').Router();

module.exports = router;

// {name:"Pollux", breed:"ugly", color:"yeark"}

const dogs = [];

router.get('/', function (request, response) {
  response.json(dogs);
})

router.post('/dogs', function (request, response) {
  dogs.push(request.body);
  response.statusCode(201).json(request. body);
})

